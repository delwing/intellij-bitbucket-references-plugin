package com.atlassian.bitbucket.linky.util;

import com.atlassian.bitbucket.linky.LinesSelection;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static com.atlassian.bitbucket.linky.util.LineSelections.collectLinesSelections;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LineSelectionsTest {

    @Test
    public void testEmptySelectionsList() {
        assertThat(collectWithDefaultDelimiters(emptyList()), is(empty()));
    }

    @Test
    public void testMergeAdjacentIntervals() {
        assertThat(
                collectWithDefaultDelimiters(
                        ImmutableList.of(
                                LinesSelection.of(1, 2),
                                LinesSelection.of(2, 3))),
                is(of("1:3")));

        assertThat(
                collectWithDefaultDelimiters(
                        ImmutableList.of(
                                LinesSelection.of(1, 1),
                                LinesSelection.of(2, 2),
                                LinesSelection.of(4, 4))),
                is(of("1:2,4")));
    }

    @Test
    public void testMergeOverlappingIntervals() {
        assertThat(
                collectWithDefaultDelimiters(
                        ImmutableList.of(
                                LinesSelection.of(1, 3),
                                LinesSelection.of(2, 4))),
                is(of("1:4")));

        assertThat(
                collectWithDefaultDelimiters(
                        ImmutableList.of(
                                LinesSelection.of(1, 6),
                                LinesSelection.of(3, 8),
                                LinesSelection.of(2, 4),
                                LinesSelection.of(4, 4))),
                is(of("1:8")));
    }

    @Test
    public void testNoDuplicateIntervalsInUrl() {
        assertThat(
                collectWithDefaultDelimiters(
                        ImmutableList.of(
                                LinesSelection.of(14, 16),
                                LinesSelection.of(19, 23),
                                LinesSelection.of(19, 20),
                                LinesSelection.of(21, 23),
                                LinesSelection.of(19, 23),
                                LinesSelection.of(25, 25),
                                LinesSelection.of(25, 25),
                                LinesSelection.of(27, 31))),
                is(of("14:16,19:23,25,27:31")));
    }

    @Test
    public void testOrderIntervals() {
        assertThat(
                collectWithDefaultDelimiters(
                        ImmutableList.of(
                                LinesSelection.of(12, 14),
                                LinesSelection.of(1, 2),
                                LinesSelection.of(22, 34),
                                LinesSelection.of(10, 10))),
                is(of("1:2,10,12:14,22:34")));
    }

    @Test
    public void testPrefix() {
        String prefix = "PREFIX = [";
        String selectionsDelimiter = "];[";
        String intervalSign = "..";
        String suffix = "]";
        assertThat(
                collectLinesSelections(ImmutableList.of(
                        LinesSelection.of(1, 2),
                        LinesSelection.of(5, 5),
                        LinesSelection.of(8, 9)),
                        prefix,
                        selectionsDelimiter,
                        intervalSign,
                        suffix),
                is(of(String.format("%1$s1%3$s2%2$s5%2$s8%3$s9%4$s", prefix, selectionsDelimiter, intervalSign, suffix))));
    }

    private Optional<String> collectWithDefaultDelimiters(List<LinesSelection> linesSelections) {
        return collectLinesSelections(linesSelections, "", ",", ":", "");
    }
}