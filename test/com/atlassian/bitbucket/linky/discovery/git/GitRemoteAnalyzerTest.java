package com.atlassian.bitbucket.linky.discovery.git;

import com.atlassian.bitbucket.linky.discovery.BitbucketServerDetector;
import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.atlassian.bitbucket.linky.service.blame.BlameLineService;
import com.atlassian.bitbucket.linky.service.hosting.HostingRegistry;
import com.atlassian.bitbucket.linky.util.UriScheme;
import com.intellij.openapi.vfs.VirtualFile;
import git4idea.repo.GitRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.bitbucket.linky.Constants.BITBUCKET_CLOUD_DEV_HOST;
import static com.atlassian.bitbucket.linky.Constants.BITBUCKET_CLOUD_HOST;
import static com.atlassian.bitbucket.linky.hosting.Hosting.Type.CLOUD;
import static com.atlassian.bitbucket.linky.hosting.Hosting.Type.SERVER;
import static com.atlassian.bitbucket.linky.repository.BitbucketRepository.VcsType.GIT;
import static com.atlassian.bitbucket.linky.util.UriScheme.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GitRemoteAnalyzerTest {
    private static final String BITBUCKET_CLOUD_WITH_SUBDOMAIN_HOST = "staging." + BITBUCKET_CLOUD_HOST;
    private static final String BITBUCKET_CLOUD_DEV_WITH_SUBDOMAIN_HOST = "staging." + BITBUCKET_CLOUD_DEV_HOST;

    public static final String CLOUD_DEV_PROJECT = "https://staging.bb-inf.net/bitbucket/test/";
    public static final String CLOUD_PROJECT = "https://bitbucket.org/atlas/some-project/";
    public static final String CLOUD_WITH_SUBDOMAIN_PROJECT = "https://staging.bitbucket.org/atlas/some-project/";
    public static final String SERVER_HOST = "some-host.com";
    public static final String SERVER_APP_PATH = "/some/path";

    @Mock
    private BlameLineService blameLineService;
    @Mock
    private Hosting hosting;
    @Mock
    private HostingRegistry hostingRegistry;
    @Mock
    private BitbucketServerDetector bitbucketServerDetector;
    @InjectMocks
    private GitRemoteAnalyzer remoteAnalyzer;
    @Mock
    private GitRepository repository;
    @Mock
    private VirtualFile root;

    @Before
    public void setup() {
        when(bitbucketServerDetector.discoverBitbucketServer(isA(UriScheme.class), anyString(), anyInt(), anyString())).thenReturn(empty());

        when(hosting.getType()).thenReturn(CLOUD);
        when(hosting.getBaseRestUri()).thenReturn(empty());

        when(hostingRegistry.registerBitbucketCloud(anyString())).thenAnswer(
                invocation -> {
                    String host = invocation.getArgumentAt(0, String.class);

                    String baseUriString = "invalid-cloud-host-provided";
                    if (BITBUCKET_CLOUD_HOST.equals(host) ||
                            BITBUCKET_CLOUD_WITH_SUBDOMAIN_HOST.equals(host) ||
                            BITBUCKET_CLOUD_DEV_WITH_SUBDOMAIN_HOST.equals(host)) {
                        baseUriString = host;
                    }

                    when(hosting.getBaseUri()).thenReturn(URI.create("https://" + baseUriString + "/"));
                    return of(hosting);
                });

        when(hostingRegistry.getBitbucketServer(any(), any(), anyInt(), any())).thenReturn(empty());
        when(hostingRegistry.registerBitbucketServer(any(), anyString(), anyInt(), any(), any())).thenReturn(empty());

        when(repository.getRoot()).thenReturn(root);
    }

    @Test
    public void testCloudDevHttp() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "https://user@staging.bb-inf.net/bitbucket/test.git");

        assertRepository(maybeRepository, CLOUD_DEV_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_DEV_WITH_SUBDOMAIN_HOST);
    }

    @Test
    public void testCloudDevSsh() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "git-staging@staging.bb-inf.net:bitbucket/test.git");

        assertRepository(maybeRepository, CLOUD_DEV_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_DEV_WITH_SUBDOMAIN_HOST);
    }


    @Test
    public void testCloudHttp() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "http://user@bitbucket.org/atlas/some-project.git");

        assertRepository(maybeRepository, CLOUD_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_HOST);
    }

    @Test
    public void testCloudHttps() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "https://user@bitbucket.org/atlas/some-project.git");

        assertRepository(maybeRepository, CLOUD_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_HOST);
    }

    @Test
    public void testCloudSsh() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "ssh://git@bitbucket.org:atlas/some-project.git");

        assertRepository(maybeRepository, CLOUD_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_HOST);
    }

    @Test
    public void testCloudSshWithoutScheme() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "git@bitbucket.org:atlas/some-project.git");

        assertRepository(maybeRepository, CLOUD_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_HOST);
    }

    @Test
    public void testCloudSshWithoutSuffix() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "ssh://git@bitbucket.org:atlas/some-project");

        assertRepository(maybeRepository, CLOUD_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_HOST);
    }

    @Test
    public void testCloudSshWithoutSchemeWithCustomUser() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "git-staging@bitbucket.org:atlas/some-project");

        assertRepository(maybeRepository, CLOUD_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_HOST);
    }

    @Test
    public void testCloudSshWithoutSchemeWithCustomUserAndSubdomain() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "git-staging@staging.bitbucket.org:atlas/some-project");

        assertRepository(maybeRepository, CLOUD_WITH_SUBDOMAIN_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_WITH_SUBDOMAIN_HOST);
    }

    @Test
    public void testCloudSshWithSubdomain() {
        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "git@staging.bitbucket.org:atlas/some-project");

        assertRepository(maybeRepository, CLOUD_WITH_SUBDOMAIN_PROJECT);
        verify(hostingRegistry, times(1)).registerBitbucketCloud(BITBUCKET_CLOUD_WITH_SUBDOMAIN_HOST);
    }

    @Test
    public void testServerHttp() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "http://user@some-host.com/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(HTTP, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerHttpWithoutSuffix() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "http://user@some-host.com/atlas/some-project");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(HTTP, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerHttpWithAppPath() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "http://git@some-host.com/some/path/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(HTTP, SERVER_HOST, -1, SERVER_APP_PATH);
    }

    @Test
    public void testServerHttpWithPortAndAppPath() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "http://git@some-host.com:7999/some/path/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(HTTP, SERVER_HOST, 7999, SERVER_APP_PATH);
    }

    @Test
    public void testServerHttps() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "https://git@some-host.com/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(HTTPS, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerSsh() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "ssh://git@some-host.com/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(SSH, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerSshWithoutScheme() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "git@some-host.com/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(SSH, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerSshWithoutSchemeWithCustomUser() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "git@some-host.com/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(SSH, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerSshWithoutSuffix() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "git@some-host.com/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(SSH, SERVER_HOST, -1, "");
    }

    @Test
    public void testServerSshWithAndAppPath() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "ssh://git@some-host.com/some/path/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(SSH, SERVER_HOST, -1, SERVER_APP_PATH);
    }

    @Test
    public void testServerSshWithPortAndAppPath() {
        remoteAnalyzer.bitbucketRepositoryFor(repository, "ssh://git@some-host.com:1234/some/path/atlas/some-project.git");

        verify(bitbucketServerDetector, times(1)).discoverBitbucketServer(SSH, SERVER_HOST, 1234, SERVER_APP_PATH);
    }

    @Test
    public void testServerNormalRepository() {
        URI uri = URI.create("https://some-host.com:1234/some/path/");

        when(bitbucketServerDetector.discoverBitbucketServer(SSH, "some-host.com", 1234, "/some/path")).thenReturn(of(uri));
        when(hostingRegistry.registerBitbucketServer(eq(SSH), eq("some-host.com"), eq(1234), eq("/some/path"), same(uri))).thenReturn(of(hosting));
        when(hosting.getBaseUri()).thenReturn(uri);

        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository, "git@some-host.com:1234/some/path/atlas/some-project");

        assertRepository(maybeRepository, "https://some-host.com:1234/some/path/projects/atlas/repos/some-project/");
    }

    @Test
    public void testServerUserRepository() {
        URI uri = URI.create("https://some-host.com:7999/some/path/");
        when(bitbucketServerDetector.discoverBitbucketServer(SSH, "some-host.com", 7999, "/some/path")).thenReturn(of(uri));
        when(hostingRegistry.registerBitbucketServer(eq(SSH), eq("some-host.com"), eq(7999), eq("/some/path"), same(uri)))
                .thenReturn(of(hosting));
        when(hosting.getBaseUri()).thenReturn(uri);

        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository,
                        "ssh://git@some-host.com:7999/some/path/~dpenkin/test-repository.git");

        assertRepository(maybeRepository, "https://some-host.com:7999/some/path/users/dpenkin/repos/test-repository/");
    }

    @Test
    public void testNoDiscoveryWhenServerCached() {
        when(hosting.getType()).thenReturn(SERVER);
        when(hosting.getBaseUri()).thenReturn(URI.create("https://some-host.com:1234/some/path/"));
        when(hostingRegistry.getBitbucketServer(SSH, "some-host.com", 1234, "/some/path")).thenReturn(of(hosting));

        Optional<BitbucketRepository> maybeRepository =
                remoteAnalyzer.bitbucketRepositoryFor(repository,
                        "ssh://git@some-host.com:1234/some/path/atlas/some-project.git");

        assertRepository(maybeRepository, "https://some-host.com:1234/some/path/projects/atlas/repos/some-project/");

        verifyZeroInteractions(bitbucketServerDetector);
    }

    private void assertRepository(Optional<BitbucketRepository> maybeRepository, String expectedUri) {
        assertThat(maybeRepository.isPresent(), is(true));

        BitbucketRepository repo = maybeRepository.get();
        assertThat(repo.getHosting(), sameInstance(hosting));
        assertThat(repo.getName(), is(empty()));
        assertThat(repo.getRemoteName(), is(empty()));
        assertThat(repo.getRootDirectory(), is(root));
        assertThat(repo.getUri(), is(URI.create(expectedUri)));
        assertThat(repo.getVcsType(), is(GIT));
    }
}