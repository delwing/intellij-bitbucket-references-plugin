# Bitbucket Linky plugin for IntelliJ Platform #

**Bitbucket/Stash References** plugin is now called **Bitbucket Linky**

This is a plugin for [IntelliJ IDEA Platform](https://www.jetbrains.com/idea/plugins/) that provides a handy way to navigate to
[Atlassian Bitbucket](https://www.atlassian.com/software/bitbucket).

It adds following IDE actions for projects hosted on Bitbucket:

* copy Bitbucket link to the selected file or selected lines in the file (`Ctrl+Shift+X,C` on Windows / `Cmd+Shift+X,C` on Mac)
* open selected file or selected lines in the file in Bitbucket in your Browser (`Ctrl+Shift+X,B` on Windows / `Cmd+Shift+X,B` on Mac)
* open selected commit in Bitbucket in your Browser (from the gutter context menu)
* open Create pull request form in Bitbucket for current branch (`Ctrl+Shift+X,P` on Windows / `Cmd+Shift+X,P` on Mac)

Bitbucket Linky supports both **Bitbucket Cloud** and **Bitbucket Server** (formerly known as **Stash**).
                
The plugin uses your project's VCS settings (supports both Git and Mercurial) to build links 
to the repository in Bitbucket, hence no configuration is needed.

#### Please note that this plugin is not officially supported by Atlassian.

## Development

To build the plugin:

* install `gradle`
* run `gradle clean build`

To run IDEA with the plugin:

* run `gradle runIdea` or use that gradle task to create a run/debug configuration in IDEA

## License and Copyright

Please see the included `license.txt` file for details

Portions of this code are derived from [Crucible4IDEA](https://github.com/ktisha/Crucible4IDEA/) 
and are copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)

Huge thanks to [**Brent Plump**](https://bitbucket.org/silentj) and [**Piotr Wilczyński**](https://bitbucket.org/delwing)!
