package com.atlassian.bitbucket.idea.components;

import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.WritingAccessProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class PullRequestModeWritingAccessProvider extends WritingAccessProvider {

    private final PullRequestService pullRequestService;

    public PullRequestModeWritingAccessProvider(Project project) {
        pullRequestService = ServiceManager.getService(project, PullRequestService.class);
    }

    @NotNull
    @Override
    public Collection<VirtualFile> requestWriting(VirtualFile... files) {
        if (isPullRequestReviewModeEnabled()) {
            return Arrays.asList(files);
        }
        return Collections.emptyList();
    }

    private boolean isPullRequestReviewModeEnabled() {
        return pullRequestService.getActivePullRequest() != null;
    }

    @Override
    public boolean isPotentiallyWritable(@NotNull VirtualFile file) {
        return !isPullRequestReviewModeEnabled();
    }
}
