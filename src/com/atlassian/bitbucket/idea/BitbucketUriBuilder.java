package com.atlassian.bitbucket.idea;

import com.atlassian.bitbucket.linky.LinesSelection;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.List;

public interface BitbucketUriBuilder {

    URI repositoryRestUri();

    @Nullable
    URI commitView(String commitRef);

    @Nullable
    URI sourceView();

    @Nullable
    URI sourceViewOfFile(@NotNull VirtualFile file, @NotNull String vcsRef);

    @Nullable
    URI sourceViewOfFileWithSelectedLines(@NotNull VirtualFile file, @NotNull List<LinesSelection> linesSelections, @NotNull String vcsRef);
}
