package com.atlassian.bitbucket.idea.pullrequest;

import java.net.URI;

public interface User {

    String getDisplayName();

    String getSlug();

    URI getUserUrl();

    URI getAvatarUrl(int avatarSize);
}
