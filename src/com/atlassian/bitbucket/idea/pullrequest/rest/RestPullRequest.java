package com.atlassian.bitbucket.idea.pullrequest.rest;

import java.util.Date;

/*
 * Represents Pull Request:
 *
 * {
 *     "author": {
 *         "approved": false,
 *         "role": "AUTHOR",
 *         "status": "UNAPPROVED",
 *         "user": {
 *             "active": true,
 *             "displayName": "Michael Studman",
 *             "emailAddress": "mstudman@atlassian.com",
 *             "id": 20,
 *             "links": {
 *                 "self": [
 *                     {
 *                         "href": "https://stash.dev.internal.atlassian.com/users/mstudman"
 *                     }
 *                 ]
 *             },
 *             "name": "mstudman",
 *             "slug": "mstudman",
 *             "type": "NORMAL"
 *         }
 *     },
 *     "closed": true,
 *     "createdDate": 1449441151000,
 *     "fromRef": {
 *         "displayId": "BSERVDEV-11381-ensure-gc.auto-read-from-correct-section-unit-tests",
 *         "id": "refs/heads/BSERVDEV-11381-ensure-gc.auto-read-from-correct-section-unit-tests",
 *         "latestCommit": "6986e144e07b6dca67b08b6627866668907ccd6c",
 *         "repository": {
 *             "forkable": true,
 *             "id": 2416,
 *             "links": {
 *                 "clone": [
 *                     {
 *                         "href": "ssh://git@stash.dev.internal.atlassian.com:7999/stash/stash.git",
 *                         "name": "ssh"
 *                     },
 *                     {
 *                         "href": "https://dpenkin@stash.dev.internal.atlassian.com/scm/stash/stash.git",
 *                         "name": "http"
 *                     }
 *                 ],
 *                 "self": [
 *                     {
 *                         "href": "https://stash.dev.internal.atlassian.com/projects/STASH/repos/stash/browse"
 *                     }
 *                 ]
 *             },
 *             "name": "Stash",
 *             "project": {
 *                 "description": "Source, Source, Baby! Please don't create test repositories in this project!",
 *                 "id": 2443,
 *                 "key": "STASH",
 *                 "links": {
 *                     "self": [
 *                         {
 *                             "href": "https://stash.dev.internal.atlassian.com/projects/STASH"
 *                         }
 *                     ]
 *                 },
 *                 "name": "Stash",
 *                 "public": false,
 *                 "type": "NORMAL"
 *             },
 *             "public": false,
 *             "scmId": "git",
 *             "slug": "stash",
 *             "state": "AVAILABLE",
 *             "statusMessage": "Available"
 *         }
 *     },
 *     "id": 7066,
 *     "links": {
 *         "self": [
 *             {
 *                 "href": "https://stash.dev.internal.atlassian.com/projects/STASH/repos/stash/pull-requests/7066"
 *             }
 *         ]
 *     },
 *     "locked": false,
 *     "open": false,
 *     "participants": [],
 *     "reviewers": [
 *         {
 *             "approved": true,
 *             "role": "REVIEWER",
 *             "status": "APPROVED",
 *             "user": {
 *                 "active": true,
 *                 "displayName": "Bryan Turner",
 *                 "emailAddress": "bturner@atlassian.com",
 *                 "id": 7,
 *                 "links": {
 *                     "self": [
 *                         {
 *                             "href": "https://stash.dev.internal.atlassian.com/users/bturner"
 *                         }
 *                     ]
 *                 },
 *                 "name": "bturner",
 *                 "slug": "bturner",
 *                 "type": "NORMAL"
 *             }
 *         },
 *         {
 *             "approved": true,
 *             "role": "REVIEWER",
 *             "status": "APPROVED",
 *             "user": {
 *                 "active": true,
 *                 "displayName": "Michael Heemskerk",
 *                 "emailAddress": "mheemskerk@atlassian.com",
 *                 "id": 5,
 *                 "links": {
 *                     "self": [
 *                         {
 *                             "href": "https://stash.dev.internal.atlassian.com/users/mheemskerk"
 *                         }
 *                     ]
 *                 },
 *                 "name": "mheemskerk",
 *                 "slug": "mheemskerk",
 *                 "type": "NORMAL"
 *             }
 *         }
 *     ],
 *     "state": "MERGED",
 *     "title": "BSERVDEV-11381: Unit tests for bug fix to ensure the right [gc] section is read when determining if gc should run",
 *     "toRef": {
 *         "displayId": "master",
 *         "id": "refs/heads/master",
 *         "latestCommit": "21ef58374770b571c235b4e414f573ff5cddcc30",
 *         "repository": {
 *             "forkable": true,
 *             "id": 2416,
 *             "links": {
 *                 "clone": [
 *                     {
 *                         "href": "ssh://git@stash.dev.internal.atlassian.com:7999/stash/stash.git",
 *                         "name": "ssh"
 *                     },
 *                     {
 *                         "href": "https://dpenkin@stash.dev.internal.atlassian.com/scm/stash/stash.git",
 *                         "name": "http"
 *                     }
 *                 ],
 *                 "self": [
 *                     {
 *                         "href": "https://stash.dev.internal.atlassian.com/projects/STASH/repos/stash/browse"
 *                     }
 *                 ]
 *             },
 *             "name": "Stash",
 *             "project": {
 *                 "description": "Source, Source, Baby! Please don't create test repositories in this project!",
 *                 "id": 2443,
 *                 "key": "STASH",
 *                 "links": {
 *                     "self": [
 *                         {
 *                             "href": "https://stash.dev.internal.atlassian.com/projects/STASH"
 *                         }
 *                     ]
 *                 },
 *                 "name": "Stash",
 *                 "public": false,
 *                 "type": "NORMAL"
 *             },
 *             "public": false,
 *             "scmId": "git",
 *             "slug": "stash",
 *             "state": "AVAILABLE",
 *             "statusMessage": "Available"
 *         }
 *     },
 *     "updatedDate": 1449576040000,
 *     "version": 22
 * }
 */
public class RestPullRequest {
    private Author author;
    private Date createdDate;
    private long id;
    private String title;

    public RestUser getAuthor() {
        return author == null ? null : author.user;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    private static class Author {
        private RestUser user;
    }
}
