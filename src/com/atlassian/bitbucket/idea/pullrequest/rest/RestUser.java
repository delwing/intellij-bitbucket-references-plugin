package com.atlassian.bitbucket.idea.pullrequest.rest;

import java.util.List;

/*
 * Represents the commentator (so far):
 *
 * {
 *     "name": "dpenkin",
 *     "emailAddress": "dpenkin@atlassian.com",
 *     "id": 59458,
 *     "displayName": "Daniil Penkin",
 *     "active": true,
 *     "slug": "dpenkin",
 *     "type": "NORMAL",
 *     "links": {
 *         "self": [
 *             {
 *                 "href": "https://stash.dev.internal.atlassian.com/users/dpenkin"
 *             }
 *         ]
 *     }
 * }
 */
public class RestUser {
    private String name;
    private String displayName;
    private String slug;
    private Links links;

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getSlug() {
        return slug;
    }

    public String getLink() {
        return links == null ? null : links.getFirstLink();
    }


    private static class Links {
        private List<Link> self;

        public String getFirstLink() {
            if (self != null && self.size() > 0) {
                Link linkItem = self.get(0);
                return linkItem == null ? null : linkItem.href;
            }
            return null;
        }
    }

    private static class Link {
        private String href;
    }
}
