package com.atlassian.bitbucket.idea.pullrequest.rest;

import java.io.IOException;
import java.util.List;

public interface PullRequestRestApi {

    List<RestPullRequest> getPullRequests() throws IOException;

    RestPullRequest getPullRequest(long pullRequestId) throws IOException;

    List<RestChangedFile> getChanges(long pullRequestId) throws IOException;

    List<RestComment> getComments(long pullRequestId, String path) throws IOException;

    RestDiff getDiff(long pullRequestId, String path) throws IOException;

    void postComment(long pullRequestId, RestPostComment postComment) throws IOException;

    boolean approve(long pullRequestId) throws IOException;

    boolean unapprove(long pullRequestId) throws IOException;
}
