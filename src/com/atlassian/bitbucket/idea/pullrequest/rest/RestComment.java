package com.atlassian.bitbucket.idea.pullrequest.rest;

import java.util.Date;
import java.util.List;

/*
 * Represents a comment:
 *
 * {
 *     "properties": {
 *         "repositoryId": 8456
 *     },
 *     "id": 131703,
 *     "version": 0,
 *     "text": "Hey guys, I want to comment here too.",
 *     "author": {
 *         ...
 *     },
 *     "createdDate": 1449532120000,
 *     "updatedDate": 1449532120000,
 *     "comments": [],
 *     "anchor": {
 *         ...
 *     },
 *     "tasks": [],
 *     "permittedOperations": {
 *         "editable": true,
 *         "deletable": true
 *     }
 * }
 */
public class RestComment {
    private long id;
    private int version;
    private String text;
    private String html;
    private RestUser author;
    private Date createdDate;
    private Date updatedDate;
    private List<RestComment> comments;
    private RestCommentAnchor anchor;
    private PermittedOperations permittedOperations;

    public long getId() {
        return id;
    }

    public int getVersion() {
        return version;
    }

    public String getText() {
        return text;
    }

    public String getHtml() {
        return html;
    }

    public RestUser getAuthor() {
        return author;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public List<RestComment> getReplies() {
        return comments;
    }

    public RestCommentAnchor getAnchor() {
        return anchor;
    }

    void setAnchor(RestCommentAnchor anchor) {
        this.anchor = anchor;
    }

    public boolean isEditable() {
        return permittedOperations != null && permittedOperations.editable;
    }

    public boolean isDeletable() {
        return permittedOperations != null && permittedOperations.deletable;
    }


    private static class PermittedOperations {
        private boolean editable;
        private boolean deletable;
    }
}
