package com.atlassian.bitbucket.idea.pullrequest.change;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.rest.PullRequestRestApi;
import com.atlassian.bitbucket.idea.pullrequest.rest.RestChangedFile;
import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.intellij.openapi.diagnostic.Logger;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class DefaultChangedFilesProvider implements ChangedFilesProvider {
    private static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);

    private static final Function<RestChangedFile, ChangedFile> CONVERT_FROM_REST = new Function<RestChangedFile, ChangedFile>() {
        @Override
        public ChangedFile apply(RestChangedFile restChangedFile) {
            return DefaultChangedFile.builder()
                    .name(restChangedFile.getName())
                    .changeType(ChangedFile.ChangeType.valueOf(restChangedFile.getChangeType()))
                    .relativePath(restChangedFile.getRelativePath())
                    .activeCommentsNumber(restChangedFile.getActiveCommentsNumber())
                    .orphanedCommentsNumber(restChangedFile.getOrphanedCommentsNUmber())
                    .build();
        }
    };

    private final PullRequestRestApi restApi;
    private final long pullRequestId;

    public DefaultChangedFilesProvider(PullRequestRestApi restApi, long pullRequestId) {
        this.restApi = restApi;
        this.pullRequestId = pullRequestId;
    }

    @Override
    public List<ChangedFile> getChanges() {
        try {
            List<RestChangedFile> pullRequests = restApi.getChanges(pullRequestId);
            return FluentIterable.from(pullRequests)
                    .transform(CONVERT_FROM_REST)
                    .toList();
        } catch (IOException e) {
            // TODO handle exceptions (cache?)
            log.error("Failed to retrieve changes for pull requests", e);
            return Collections.emptyList();
        }
    }
}
