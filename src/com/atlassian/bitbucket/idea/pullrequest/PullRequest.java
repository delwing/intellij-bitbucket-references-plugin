package com.atlassian.bitbucket.idea.pullrequest;

import java.util.Date;

public interface PullRequest {

    User getAuthor();

    Date getCreatedDate();

    long getId();

    String getTitle();
}
