package com.atlassian.bitbucket.idea.pullrequest.comment;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.DefaultUser;
import com.atlassian.bitbucket.idea.pullrequest.rest.*;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

public class DefaultCommentsProvider implements CommentsProvider {
    private static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);

    private static final Function<RestComment, Comment> CONVERT_FROM_REST = new Function<RestComment, Comment>() {
        @Override
        public Comment apply(RestComment restComment) {
            RestUser author = restComment.getAuthor();

            DefaultUser.Builder userBuilder = DefaultUser.builder()
                    .displayName(author.getDisplayName())
                    .slug(author.getSlug());
            if (author.getLink() != null) {
                userBuilder.userUri(URI.create(author.getLink()));
            }
            DefaultComment.Builder builder = DefaultComment.builder()
                    .id(restComment.getId())
                    .commentator(userBuilder.build())
                    .createdDate(restComment.getCreatedDate())
                    .isDeletable(restComment.isDeletable())
                    .isEditable(restComment.isEditable())
                    .replies(FluentIterable.from(restComment.getReplies())
                            .transform(this)
                            .toList())
                    .text(restComment.getText())
                    .htmlText(restComment.getHtml())
                    .updatedDate(restComment.getUpdatedDate())
                    .version(restComment.getVersion());

            RestCommentAnchor restAnchor = restComment.getAnchor();
            if (restAnchor != null) {
                builder.anchor(DefaultCommentAnchor.anchor(
                        restAnchor.getLine(),
                        CommentAnchor.FileType.valueOf(restAnchor.getFileType()),
                        CommentAnchor.LineType.valueOf(restAnchor.getLineType())));
            }
            return builder.build();
        }
    };

    private final PullRequestRestApi restApi;
    private final long pullRequestId;
    private final String path;

    public DefaultCommentsProvider(PullRequestRestApi restApi, long pullRequestId, String path) {
        this.restApi = restApi;
        this.pullRequestId = pullRequestId;
        this.path = path;
    }

    @NotNull
    @Override
    public List<Comment> getComments() {
        try {
            List<RestComment> comments = restApi.getComments(pullRequestId, path);
            return FluentIterable.from(comments)
                    .transform(CONVERT_FROM_REST)
                    .toList();
        } catch (IOException e) {
            // TODO handle exceptions (cache?)
            log.error("Failed to retrieve comments", e);
            return Collections.emptyList();
        }
    }

    @NotNull
    @Override
    public List<Comment> getComments(final int lineNumber) {
        return FluentIterable.from(getComments()).filter(new Predicate<Comment>() {
            @Override
            public boolean apply(Comment comment) {
                CommentAnchor anchor = comment.getAnchor();
                return anchor != null && anchor.getLineNumber() == lineNumber;
            }
        }).toList();
    }

    @Override
    public void replyToComment(@NotNull Comment comment, String text) {
        try {
            restApi.postComment(pullRequestId, RestPostComment.reply(comment.getId(), text));
        } catch (IOException e) {
            log.error("Failed to post comment", e);
            throw new RuntimeException("Failed to post comment", e);
        }
    }
}
