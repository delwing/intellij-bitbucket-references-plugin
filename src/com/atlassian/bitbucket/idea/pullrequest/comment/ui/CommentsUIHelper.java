package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.User;
import com.atlassian.bitbucket.idea.pullrequest.comment.Comment;
import com.atlassian.bitbucket.idea.pullrequest.comment.model.LineCommentsBlock;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CommentsUIHelper {

    private CommentsUIHelper() {
    }

    public static JPanel buildCommentsBlockPanel(LineCommentsBlock lineCommentsBlock) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        for (Comment comment : lineCommentsBlock.getComments()) {
            panel.add(renderComment(comment, lineCommentsBlock));
        }
        return panel;
    }

    private static JComponent renderComment(Comment comment, LineCommentsBlock lineCommentsBlock) {
        return renderComment(comment, lineCommentsBlock, 0);
    }

    private static JComponent renderComment(final Comment comment, final LineCommentsBlock lineCommentsBlock, int depth) {
        final JPanel commentWithChildrenPanel = new JPanel();
        commentWithChildrenPanel.setLayout(new BoxLayout(commentWithChildrenPanel, BoxLayout.Y_AXIS));

        User user = comment.getCommentator();

        StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
        sb.append("<b>").append(user.getDisplayName()).append(": </b>");
        if (comment.getHtmlText() != null) {
            sb.append(comment.getHtmlText());
        } else {
            sb.append(comment.getText());
        }
        sb.append("</body></html>");
        JLabel label = new JLabel();
        label.setText(sb.toString());

        JPanel commentPanel = new JPanel();
        commentPanel.setBorder(BorderFactory.createEmptyBorder(5, 5 + depth * 20, 5, 5));
        commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.X_AXIS));
        commentPanel.add(label);

        final JButton button = new JButton();
        button.setAction(new AbstractAction("Reply") {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String text = (String) JOptionPane.showInputDialog(
                        button,
                        "Enter comment text:",
                        "Reply to comment",
                        JOptionPane.QUESTION_MESSAGE,
                        Constants.BITBUCKET_ICON,
                        null,
                        null);

                if ((text != null) && (text.length() > 0)) {
                    lineCommentsBlock.replyToComment(comment, text);
                    // TODO invoke redraw
                }
            }
        });
        commentPanel.add(button);

        commentWithChildrenPanel.add(commentPanel);
        for (Comment reply : comment.getReplyComments()) {
            commentWithChildrenPanel.add(renderComment(reply, lineCommentsBlock, depth + 1));
        }

        return commentWithChildrenPanel;
    }
}
