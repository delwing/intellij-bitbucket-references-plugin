package com.atlassian.bitbucket.idea.actions;

import com.atlassian.bitbucket.idea.dialog.GitPullRequestDialog;
import com.atlassian.bitbucket.idea.pullrequest.PullRequest;
import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.intellij.dvcs.DvcsUtil;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.application.AccessToken;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vcs.VcsException;
import com.intellij.openapi.vcs.changes.Change;
import com.intellij.openapi.vcs.changes.ChangeListManager;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import git4idea.GitUtil;
import git4idea.GitVcs;
import git4idea.actions.GitRepositoryAction;
import git4idea.commands.Git;
import git4idea.commands.GitCommandResult;
import git4idea.commands.GitLineHandlerAdapter;
import git4idea.commands.GitLineHandlerListener;
import git4idea.repo.GitRemote;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryManager;
import git4idea.reset.GitResetMode;
import git4idea.util.GitUIUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/*
 * This file was derived from code in https://github.com/ktisha/Crucible4IDEA/
 *
 * Copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class GitPullRequestAction extends GitRepositoryAction {

    @Override
    @NotNull
    protected String getActionName() {
        return "Review Pull Request";
    }

    protected void perform(@NotNull final Project project,
                           @NotNull final List<VirtualFile> gitRoots,
                           @NotNull final VirtualFile defaultRoot,
                           final Set<VirtualFile> affectedRoots,
                           final List<VcsException> exceptions) throws VcsException {

        ChangeListManager changeListManager = ChangeListManager.getInstance(project);
        Collection<Change> allChanges = changeListManager.getAllChanges();
        if (allChanges.size() != 0) {
            showError(project);
            return;
        }

        if (gitRoots.size() > 1) {
            showError(project, "Unable to work with multiple git roots.");
            return;
        }

        final GitRepositoryManager repositoryManager = GitUtil.getRepositoryManager(project);
        final GitRepository repository = repositoryManager.getRepositoryForRoot(defaultRoot);
        if (repository == null) {
            showError(project, "Unable to get repository for git root.");
            return;
        }

        final GitRemote remote = GitUtil.findRemoteByName(repository, "origin");
        if (remote == null) {
            showError(project, "Couldn't find remote named 'origin'");
            return;
        }

        // TODO: In order to get the benefit of the credentials cache, I really want to use the http(s) remote
        final String remoteUrl = remote.getFirstUrl();
        if (remoteUrl == null) {
            showError(project, "URL to remote was null");
            return;
        }

        final PullRequest pullRequest = displayDialog(project, repository);
        if (pullRequest == null) {
            return;
        }

        new Task.Backgroundable(project, "Setting up Pull Request Review", false) {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                final Git git = ServiceManager.getService(Git.class);
                AccessToken accessToken = workingTreeChangeStarted(project);
                try {
                    final String remoteRef = String.format("refs/pull-requests/%d/merge", pullRequest.getId());
                    final String localRef = String.format("pull-requests/%d/merge", pullRequest.getId());

                    GitLineHandlerAdapter lineListener = new GitLineHandlerAdapter();
                    final List<GitLineHandlerListener> lineListeners = Collections.<GitLineHandlerListener>singletonList(lineListener);

                    // TODO: I should probably check that the ref exists on the remote... (though we get a nice error dialog if it doesn't)

                    // git branch --delete -f pull-requests/<PR_ID>/merge
                    // We don't care if this fails (which it will if the ref doesn't already exist) so don't check the result code
                    git.branchDelete(repository, localRef, true, lineListener);

                    // TODO: The fetch will fail if we're already on the branch.  It'd be nice if we aren't on the branch and just on the commit
                    // git fetch origin refs/pull-requests/<PR_ID>/merge:pull-requests/<PR_ID>/merge
                    GitCommandResult result = git.fetch(repository, remote, lineListeners, remoteRef + ":" + localRef);
                    if (!result.success()) {
                        handleError(result, project, repositoryManager, defaultRoot);
                        return;
                    }

                    // git checkout pull-requests/<PR_ID>/merge
                    result = git.checkout(repository, localRef, null, false, false, lineListener);
                    if (!result.success()) {
                        handleError(result, project, repositoryManager, defaultRoot);
                        return;
                    }

                    // git reset --soft HEAD^
                    result = git.reset(repository, GitResetMode.SOFT, "HEAD^", lineListener);
                    if (!result.success()) {
                        handleError(result, project, repositoryManager, defaultRoot);
                        return;
                    }

                    affectedRoots.add(defaultRoot);

                    handleResult(result, project, repository, affectedRoots);

                    ServiceManager.getService(project, PullRequestService.class).setActivePullRequest(pullRequest);
                } finally {
                    workingTreeChangeFinished(project, accessToken);
                }
            }

        }.queue();
    }

    private void showError(@NotNull Project project) {
        Messages.showMessageDialog(project, "You have unsaved changes.  Please save/stash your work first.", "Pull Request Review", Messages.getErrorIcon());
    }

    @Override
    protected boolean executeFinalTasksSynchronously() {
        return false;
    }

    @Nullable
    private PullRequest displayDialog(@NotNull Project project, @NotNull Repository repository) {
        final PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
        final GitPullRequestDialog dialog = new GitPullRequestDialog(project, pullRequestService, repository);
        dialog.show();

        if (!dialog.isOK()) {
            return null;
        }

        return dialog.getSelectedPullRequest();
    }

    private void handleResult(GitCommandResult result, Project project, GitRepository repository, Set<VirtualFile> affectedRoots) {
        GitRepositoryManager repositoryManager = GitUtil.getRepositoryManager(project);
        VirtualFile root = repository.getRoot();
        if (result.success()) {
            VfsUtil.markDirtyAndRefresh(false, true, false, root);
            List<VcsException> exceptions = new ArrayList<>();
            repositoryManager.updateRepository(root);
            GitVcs gitVcs = GitVcs.getInstance(project);
            if (gitVcs == null) {
                showError(project, "Git VCS is null");
                return;
            }
            runFinalTasks(project, gitVcs, affectedRoots, getActionName(), exceptions);
        } else {
            handleError(result, project, repositoryManager, root);
        }
    }

    private void handleError(GitCommandResult result, Project project, GitRepositoryManager repositoryManager, VirtualFile root) {
        GitUIUtil.notifyError(project, "Git " + getActionName() + " Failed", result.getErrorOutputAsJoinedString(), true, asException(result.getException()));
        repositoryManager.updateRepository(root);
    }

    private void showError(Project project, String message) {
        Messages.showMessageDialog(project, message, "Review Pull Request", Messages.getErrorIcon());
    }

    /**
     * Work around a change between IDEA 15 (which returns a token) and IDEA 14 (which doesn't)
     */
    @Nullable
    private AccessToken workingTreeChangeStarted(@NotNull Project project) {
        try {
            Method workingTreeChangeStartedMethod = DvcsUtil.class.getMethod("workingTreeChangeStarted", Project.class);
            Object returnValue = workingTreeChangeStartedMethod.invoke(null, project);
            if (returnValue != null && returnValue instanceof AccessToken) {
                return (AccessToken) returnValue;
            } else {
                return null;
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("Unable to mark start of working tree change", e);
        }
    }

    /**
     * Work around a change between IDEA 15 (which requires a token) and IDEA 14 (which doesn't)
     */
    private void workingTreeChangeFinished(@NotNull Project project, @Nullable AccessToken token) {
        try {
            if (token == null) {
                // IDEA 14
                Method workingTreeChangeFinishedMethod = DvcsUtil.class.getMethod("workingTreeChangeFinished", Project.class);
                workingTreeChangeFinishedMethod.invoke(null, project);
            } else {
                // IDEA 15
                Method workingTreeChangeFinishedMethod = DvcsUtil.class.getMethod("workingTreeChangeFinished", Project.class, AccessToken.class);
                workingTreeChangeFinishedMethod.invoke(null, project, token);
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("Unable to mark finish of working tree change", e);
        }
    }

    @Nullable
    private static Exception asException(Throwable throwable) {
        if (throwable == null) {
            return null;
        } else if (throwable instanceof Exception) {
            return (Exception) throwable;
        } else {
            return new RuntimeException(throwable.getMessage(), throwable);
        }
    }
}