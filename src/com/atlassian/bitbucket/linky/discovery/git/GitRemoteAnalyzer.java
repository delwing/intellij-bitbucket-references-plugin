package com.atlassian.bitbucket.linky.discovery.git;

import com.atlassian.bitbucket.linky.discovery.AbstractRemoteAnalyzer;
import com.atlassian.bitbucket.linky.discovery.BitbucketServerDetector;
import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.atlassian.bitbucket.linky.repository.CloudBitbucketRepository;
import com.atlassian.bitbucket.linky.repository.ServerBitbucketRepository;
import com.atlassian.bitbucket.linky.service.blame.BlameLineService;
import com.atlassian.bitbucket.linky.service.hosting.HostingRegistry;
import com.atlassian.bitbucket.linky.util.UriScheme;
import com.intellij.dvcs.repo.Repository;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.bitbucket.linky.repository.BitbucketRepository.VcsType.GIT;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class GitRemoteAnalyzer extends AbstractRemoteAnalyzer {
    private static final Pattern BITBUCKET_SERVER_PATH_PATTERN = Pattern.compile("(.*?)(?:/scm)?/([^/]*)/([^/]*)");
    private static final Pattern BITBUCKET_SERVER_USER_PATTERN = Pattern.compile("~(.+)");
    private static final String GIT_REPO_EXTENSION = ".git";

    private final BitbucketServerDetector bitbucketServerDetector;

    public GitRemoteAnalyzer(HostingRegistry hostingRegistry,
                             BitbucketServerDetector bitbucketServerDetector,
                             BlameLineService blameLineService) {
        super(hostingRegistry, blameLineService);
        this.bitbucketServerDetector = bitbucketServerDetector;
    }

    @NotNull
    @Override
    public Optional<BitbucketRepository> bitbucketRepositoryFor(@NotNull Repository repository, @NotNull String remote) {
        log.debug(format("Analyzing git remote url '%s'", remote));

        remote = remote.toLowerCase();

        if (!remote.contains("://")) {
            log.debug(format("Prepending remote url '%s' with ssh scheme for further analysis", remote));
            remote = "ssh://" + remote;
        }

        return parseRemote(remote)
                .flatMap(uri -> bitbucketRepositoryFor(repository, uri));
    }

    private Optional<BitbucketRepository> bitbucketRepositoryFor(Repository repository, URI remoteUri) {
        Optional<UriScheme> maybeScheme = ofNullable(remoteUri.getScheme())
                .flatMap(s -> UriScheme.forName(s.toLowerCase()));

        if (!maybeScheme.isPresent()) {
            log.warn(format("Failed to parse scheme of remote URL '%s'", remoteUri));
            return empty();
        }

        UriScheme scheme = maybeScheme.get();
        String host = remoteUri.getHost();
        int port = remoteUri.getPort();
        String authority = remoteUri.getAuthority();
        String path = remoteUri.getPath();

        log.debug(format("Found parameters: scheme '%s', host '%s', port '%d', " +
                "authority '%s', path '%s'", scheme, host, port, authority, path));

        if (path.endsWith(GIT_REPO_EXTENSION)) {
            path = path.substring(0, path.length() - GIT_REPO_EXTENSION.length());
        }

        final String finalPath = path.endsWith("/") ? path : path + "/";

        // host might be null for Bitbucket Cloud SSH URLs
        if (host == null) {
            if (authority != null && containsBitbucketCloudHost(authority)) {
                int colonIndex = authority.lastIndexOf(":");

                host = authority.substring(authority.indexOf("@") + 1, colonIndex);
                String hierarchy = authority.substring(colonIndex + 1);

                return hostingRegistry.registerBitbucketCloud(host)
                        .map(bb -> createCloudRepository(bb, repository, hierarchy + finalPath));
            }
        } else {
            if (containsBitbucketCloudHost(host)) {
                return hostingRegistry.registerBitbucketCloud(host)
                        .map(bb -> createCloudRepository(bb, repository, finalPath));
            }

            Matcher matcher = BITBUCKET_SERVER_PATH_PATTERN.matcher(path);
            if (matcher.matches()) {
                final String fHost = host;
                String appPath = matcher.group(1);
                String projectKey = matcher.group(2);
                String repoSlug = matcher.group(3);

                Optional<Hosting> maybeServer = hostingRegistry.getBitbucketServer(scheme, fHost, port, appPath);

                if (!maybeServer.isPresent()) {
                    maybeServer = bitbucketServerDetector.discoverBitbucketServer(scheme, fHost, port, appPath)
                            .flatMap(uri -> hostingRegistry.registerBitbucketServer(scheme, fHost, port, appPath, uri));
                }

                Optional<BitbucketRepository> serverBitbucketRepository = maybeServer
                        .map(bb -> createServerRepository(bb, repository, projectKey, repoSlug));

                if (!serverBitbucketRepository.isPresent()) {
                    log.info(format("Path '%s' of git remote url '%s' matches Bitbucket Server " +
                            "repository path pattern but no base URI resolved", path, remoteUri));
                }

                return serverBitbucketRepository;
            }
        }

        return empty();
    }

    private CloudBitbucketRepository createCloudRepository(Hosting hosting,
                                                           Repository repository,
                                                           String repositoryPath) {
        return CloudBitbucketRepository.builder(repository.getRoot())
                .bitbucket(hosting)
                .vcsType(GIT)
                .vcsRepository(repository)
                .blameLineService(blameLineService)
//                .name()
//                .remoteName()
                .uri(hosting.getBaseUri().resolve(repositoryPath))
                .vcsReferenceSupplier(() -> getCurrentVcsReference(repository))
                .build();
    }

    private ServerBitbucketRepository createServerRepository(Hosting hosting,
                                                             Repository repository,
                                                             String projectKey,
                                                             String repoSlug) {
        return ServerBitbucketRepository.builder(repository.getRoot())
                .bitbucket(hosting)
                .vcsType(GIT)
                .vcsRepository(repository)
                .blameLineService(blameLineService)
//                .name()
//                .remoteName()
                .uri(hosting.getBaseUri().resolve(createServerRepositoryPath(projectKey, repoSlug)))
                .vcsReferenceSupplier(() -> getCurrentVcsReference(repository))
                .build();
    }

    private String createServerRepositoryPath(String projectKey, String repoSlug) {
        Matcher matcher = BITBUCKET_SERVER_USER_PATTERN.matcher(projectKey);
        if (matcher.matches()) {
            return format("users/%s/repos/%s/", matcher.group(1), repoSlug);
        }
        return format("projects/%s/repos/%s/", projectKey, repoSlug);
    }
}
