package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.util.Optionals;
import com.atlassian.bitbucket.linky.util.UriScheme;
import com.intellij.openapi.diagnostic.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static com.atlassian.bitbucket.linky.util.UriScheme.HTTP;
import static com.atlassian.bitbucket.linky.util.UriScheme.HTTPS;
import static com.atlassian.bitbucket.linky.util.Uris.buildUri;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

public class BitbucketServerDetector {

    private static final Logger log = Logger.getInstance(LOGGER_NAME);

    private static final String BITBUCKET_SERVER_TEST_PATH = "/rest/api/latest/application-properties";
    private static final String BITBUCKET_SERVER_RESPONSE_PATTERN =
            "^\\{(?=.*\\bversion\\b)(?=.*\\bbuildNumber\\b)(?=.*\\bbuildDate\\b)(?=.*\\bdisplayName\\b).*\\}$";

    @NotNull
    public Optional<URI> discoverBitbucketServer(@NotNull UriScheme detectedScheme,
                                                 @NotNull String detectedHost,
                                                 int detectedPort,
                                                 @NotNull String detectedApplicationPath) {
        detectedHost = detectedHost.toLowerCase();
        detectedApplicationPath = detectedApplicationPath.toLowerCase();

        final String appPath = detectedApplicationPath.isEmpty() ? "/" : detectedApplicationPath;

        return getUriListToCheck(detectedScheme, detectedHost, detectedPort, detectedApplicationPath).stream()
                .filter(this::isBitbucketServerUri)
                .parallel()
                .findFirst()
                .map(uri -> uri.resolve(appPath));
    }

    private List<URI> getUriListToCheck(UriScheme detectedScheme, String host, int port, String path) {
        String fullTestPath = path + BITBUCKET_SERVER_TEST_PATH;

        if (detectedScheme == UriScheme.SSH) {
            return Stream.of(HTTP, HTTPS)
                    .flatMap(scheme -> createUriForScheme(host, port, fullTestPath, scheme))
                    .collect(toList());
        } else {
            return createUriForScheme(host, port, fullTestPath, detectedScheme)
                    .collect(Collectors.toList());
        }
    }

    private Stream<URI> createUriForScheme(String host, int port, String fullTestPath, UriScheme scheme) {
        return IntStream.of(-1, port)
                .distinct()
                .mapToObj(p -> buildUri(of(scheme), of(host), of(p), of(fullTestPath), emptyMap(), empty(),
                        e -> log.warn("Failed to prepare list of URIs to check Bitbucket Server presence", e)))
                .flatMap(Optionals::toStream);
    }

    // TODO extract transport
    private boolean isBitbucketServerUri(URI uri) {
        try {
            log.debug(format("Checking URI '%s' for Bitbucket Server presence", uri.toString()));
            HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(4000);
            connection.setReadTimeout(4000);

            if (connection.getResponseCode() < 400) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                    StringBuilder sb = new StringBuilder();

                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                    String response = sb.toString();
                    if (response.matches(BITBUCKET_SERVER_RESPONSE_PATTERN)) {
                        log.info(format("Found Bitbucket Server on URI '%s'", uri));
                        return true;
                    }
                }
            }
        } catch (IOException e) {
            log.debug(format("Error occurred while checking URI '%s'", uri), e);
        }
        return false;
    }

}
