package com.atlassian.bitbucket.linky.service;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.discovery.VcsRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;

public class DefaultBitbucketRepositoryService implements BitbucketRepositoryService {

    private final Project project;
    private final ProjectLevelVcsManager vcsManager;
    private final VcsRepositoryDiscoverer vcsRepositoryDiscoverer;

    protected DefaultBitbucketRepositoryService(Project project) {
        this.project = project;
        this.vcsManager = ProjectLevelVcsManager.getInstance(project);
        this.vcsRepositoryDiscoverer = project.getComponent(VcsRepositoryDiscoverer.class);
    }

    @Override
    public Optional<BitbucketLinky> getBitbucketLinky(@NotNull VirtualFile file) {
        return getBitbucketRepository(file).map(identity());
    }

    @Override
    public Optional<BitbucketRepository> getBitbucketRepository() {
        return ofNullable(project.getBaseDir())
                .flatMap(this::getBitbucketRepository);
    }

    @Override
    public Optional<BitbucketRepository> getBitbucketRepository(@NotNull VirtualFile file) {
        return ofNullable(vcsManager.getVcsRootFor(file))
                .map(root -> vcsRepositoryDiscoverer.discoverBitbucketRepositories().get(root));
    }


}
