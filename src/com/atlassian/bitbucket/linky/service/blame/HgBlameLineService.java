package com.atlassian.bitbucket.linky.service.blame;

import com.google.common.collect.ImmutableList;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Pair;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.vcsUtil.VcsUtil;
import org.jetbrains.annotations.NotNull;
import org.zmlx.hg4idea.execution.HgCommandExecutor;
import org.zmlx.hg4idea.execution.HgCommandResult;
import org.zmlx.hg4idea.repo.HgRepository;
import org.zmlx.hg4idea.repo.HgRepositoryManager;
import org.zmlx.hg4idea.util.HgUtil;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static com.atlassian.bitbucket.linky.util.Optionals.parseInteger;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Optional.*;

public class HgBlameLineService implements BlameLineService {
    protected static final Logger log = Logger.getInstance(LOGGER_NAME);

    private static final Pattern LINE_PATTERN = Pattern.compile("\\s*[0-9a-fA-F]+\\s+(.+):\\s*([0-9]+):\\s.*");

    private final Project project;

    public HgBlameLineService(Project project) {
        this.project = project;
    }

    @NotNull
    @Override
    public Optional<Pair<String, Integer>> getBlameLineReference(@NotNull VirtualFile file, int lineNumber) {
        HgRepositoryManager repositoryManager = HgUtil.getRepositoryManager(project);
        // index starts from 0, line number - from 1
        int lineIndex = lineNumber - 1;

        return ofNullable(repositoryManager.getRepositoryForFile(file))
                .map(repository -> runAnnotateCommand(repository, file))
                .flatMap(lines -> lines.size() >= lineIndex ? of(lines.get(lineIndex)) : empty())
                .flatMap(this::parseReferenceFromBlameOutput);
    }

    private List<String> runAnnotateCommand(HgRepository repository, VirtualFile file) {
        // hg blame -f -c -l -w svnImportScript.sh
        ImmutableList<String> arguments = ImmutableList.of("-fclw", VcsUtil.getFilePath(file).getPath());
        log.debug(format("Running command 'hg annotate %s'", arguments));

        HgCommandResult result = new HgCommandExecutor(project)
                .executeInCurrentThread(repository.getRoot(), "annotate", arguments);

        return result == null ? emptyList() : result.getOutputLines();
    }

    private Optional<Pair<String, Integer>> parseReferenceFromBlameOutput(String outputLine) {
        Matcher matcher = LINE_PATTERN.matcher(outputLine);
        if (matcher.matches()) {
            return parseInteger(matcher.group(2), e ->
                    log.error(format("Failed to parse line number from hg annotate output line '%s'", outputLine)))
                    .map(lineNumber -> Pair.create(matcher.group(1), lineNumber));
        }

        log.error(format("Hg output doesn't match expected pattern '%s': '%s'", LINE_PATTERN, outputLine));
        return empty();
    }
}
