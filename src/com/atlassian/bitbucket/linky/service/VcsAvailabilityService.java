package com.atlassian.bitbucket.linky.service;

public interface VcsAvailabilityService {

    boolean isGitAvailable();

    boolean isHgAvailable();
}
