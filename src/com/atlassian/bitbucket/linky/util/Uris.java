package com.atlassian.bitbucket.linky.util;

import org.apache.http.client.utils.URIBuilder;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public class Uris {

    private Uris() {
        throw new UnsupportedOperationException(getClass().getSimpleName() + " only contains static utility methods " +
                "and should not be instantiated.");
    }

    @NotNull
    public static Optional<URI> parseRemote(@NotNull String remote, @NotNull Consumer<URISyntaxException> onError) {
        try {
            return of(new URIBuilder(remote).build());
        } catch (URISyntaxException e) {
            onError.accept(e);
            return empty();
        }
    }

    @NotNull
    public static Optional<URI> buildUri(@NotNull Optional<UriScheme> scheme,
                                         @NotNull Optional<String> host,
                                         @NotNull Optional<Integer> port,
                                         @NotNull Optional<String> path,
                                         @NotNull Map<String, String> parameters,
                                         @NotNull Optional<String> fragment,
                                         @NotNull Consumer<URISyntaxException> onError) {
        URIBuilder builder = new URIBuilder();
        scheme.ifPresent(s -> builder.setScheme(s.toString()));
        host.ifPresent(builder::setHost);
        port.ifPresent(builder::setPort);
        path.ifPresent(builder::setPath);
        parameters.entrySet().stream()
                .forEach(entry -> builder.addParameter(entry.getKey(), entry.getValue()));
        fragment.ifPresent(builder::setFragment);

        try {
            return of(builder.build());
        } catch (URISyntaxException e) {
            onError.accept(e);
            return empty();
        }
    }
}
